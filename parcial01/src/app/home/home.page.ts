import { Component, OnInit } from '@angular/core';
import { Data } from '../models/data';
import { DataService } from '../services/data.service';
import { ReferenceAst, preserveWhitespacesDefault } from '@angular/compiler';

var t = "";
var press:any = false;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  
  data: Data[];
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.dataService.getAllData().subscribe(res => {
      console.log('Data', res);
      this.data = res;
    });
  }
  
  Validation(){
    press=true;
  }

  deValidation(){
    press=false;
  }
}
