// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCFmMcgAhf9MdYdQ_ZVzhJXBQQPtyCBiwQ",
    authDomain: "apps2-2a9f8.firebaseapp.com",
    databaseURL: "https://apps2-2a9f8.firebaseio.com",
    projectId: "apps2-2a9f8",
    storageBucket: "apps2-2a9f8.appspot.com",
    messagingSenderId: "425752224235",
    appId: "1:425752224235:web:f862a7d7a851a9f756f9ae",
    measurementId: "G-5G4P5GJXXD"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
